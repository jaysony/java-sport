## 个人运动管理平台

### 后端技术栈  
> [项目地址](https://gitee.com/jaysony/java-sport)
1. *Springboot*

2. *Jwt*

3. *Spring Security*

4. *MySQL*

5. *Redis*

### 前端技术栈
> [项目地址](https://gitee.com/jaysony/vue-sport)
1. *Vue*

2. *axios*

3. *VueX*

4. *Vue Router*


docker 安装MySQL之后启动，解决客户端连接不上的问题，阿里云安全组开放3306端口

```text
docker run --name mysql -e MYSQL_ROOT_PASSWORD=123456 -p 3306:3306 -d mysql:8.0.16
docker exec -it mysql /bin/bash
ALTER USER 'root'@'%' IDENTIFIED BY 'password' PASSWORD EXPIRE NEVER;
ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '123456';
flush privileges;
```
spring security使用步骤：
1. 引入依赖包 `spring-boot-starter-security`
2. 加入全局配置类 `com.jackson.config.security.SecurityConfig`
3. 无权限时返回策略 `com.jackson.config.security.handler.JwtAccessDeniedHandler` 
4. 认证失败返回策略`com.jackson.config.security.handler.JwtAuthenticationEntryPoint`
5. 认证策略`com.jackson.config.security.handler.JwtAuthenticationFilter`
