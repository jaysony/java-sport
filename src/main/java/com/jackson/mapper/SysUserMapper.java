package com.jackson.mapper;

import com.jackson.entity.SysMenu;
import com.jackson.entity.SysPermission;
import com.jackson.entity.SysRole;
import com.jackson.entity.SysUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysUserMapper {
    /**
     * 查询用户信息
     * @return
     */
    List<SysUser> findAll();

    /**
     *
     * @param username
     * @return
     */
    SysUser findByUsername(String username);

    /**
     * 根据id查询权限信息
     * @param userId
     * @return
     */
    List<SysRole> findRoles(@Param("userId") Long userId);

    /**
     * 据id查询菜单信息
     * @param userId
     * @return
     */
    List<SysMenu> findMenus(@Param("userId") Long userId);


    /**
     * 据父级id和用户id查询子级菜单信息
     * @param id
     * @param userId
     * @return
     */
    List<SysMenu> findChildrenMenus(@Param("id") Long id, @Param("userId") Long userId);


    /**
     * 据id查询权限数据
     * @param userId
     * @return
     */
    List<SysPermission> findPermissions(@Param("userId") Long userId);
}
