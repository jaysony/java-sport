package com.jackson.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SysUser implements UserDetails {

    @ApiModelProperty(value = "主键")
    private long id;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "登录密码")
    private String password;

    @ApiModelProperty(value = "性别")
    private Integer sex;

    @ApiModelProperty(value = "头像")
    private String avatar;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "微信唯一id")
    private String openId;

    @ApiModelProperty(value = "当前状态")
    private boolean status;

    @ApiModelProperty(value = "是否管理员")
    private boolean admin;

    @ApiModelProperty(value = "电话号码")
    private String phoneNumber;

    @ApiModelProperty(value = "角色信息")
    private List<SysRole> roles;

    @ApiModelProperty(value = "用户对应的菜单列表")
    private List<SysMenu> menus;

    @ApiModelProperty(value = "用户的权限数据")
    private List<SysPermission> permissions;


    /**
     * 权限数据
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> list = new ArrayList<>();
        if (roles != null && roles.size() > 0) {
            roles.forEach(item-> {
                list.add(new SimpleGrantedAuthority("ROLE_" + item.getCode()));
            });
        }

        if (permissions != null && permissions.size() > 0) {
            permissions.forEach(item-> {
                list.add(new SimpleGrantedAuthority( item.getCode()));
            });
        }

        return list;

//        List<GrantedAuthority> list = new ArrayList<>();
//        list.add(new SimpleGrantedAuthority("ROLE_admin"));
//        return list;
    }

    /**
     * 用户名
     * @return
     */
    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    /**
     * 是否被禁用
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return status;
    }
}
