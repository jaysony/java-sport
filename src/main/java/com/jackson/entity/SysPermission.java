package com.jackson.entity;

import lombok.Data;

@Data
public class SysPermission {
    private long id;
    private String label;
    private String code;

}
