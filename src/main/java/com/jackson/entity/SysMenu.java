package com.jackson.entity;

import lombok.Data;

import java.util.List;

@Data
public class SysMenu {
    private long id;
    /**
     * 前端路由
     */
    private String path;
    /**
     * 菜单图标
     */
    private String icon;
    /**
     * 菜单标题
     */
    private String title;
    /**
     * 前端组件
     */
    private String component;

    /**
     * 子菜单
     */
    private List<SysMenu> children;
}
