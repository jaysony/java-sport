package com.jackson.entity;

import lombok.Data;

import java.util.List;

@Data
public class SysRole {
    private long id;
    private String label;
    private String code;


}
