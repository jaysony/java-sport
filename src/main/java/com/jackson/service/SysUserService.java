package com.jackson.service;

import com.jackson.entity.SysUser;
import com.jackson.util.Result;
import com.jackson.vo.LoginVo;

public interface SysUserService {
    Result findAll();

    /**
     *
     * @param loginVo
     * @return 返回token去获取资源
     */
    Result login(LoginVo loginVo);


    /**
     * 根据用户名获取用户信息
     * @param username
     * @return
     */
    SysUser findByUsername(String username);
}
