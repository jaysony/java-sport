package com.jackson.service.impl;

import com.jackson.config.security.service.UserDetailServiceImpl;
import com.jackson.entity.SysUser;
import com.jackson.mapper.SysUserMapper;
import com.jackson.service.SysUserService;
import com.jackson.util.Result;
import com.jackson.util.TokenUtils;
import com.jackson.vo.LoginVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import java.util.HashMap;

@Service
@Slf4j
public class SysUserServiceImpl implements SysUserService {


    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private UserDetailServiceImpl userDetailsService;

    @Autowired(required = true)
    private PasswordEncoder passwordEncoder;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @Autowired
    private TokenUtils tokenUtils;

    @Override
    public Result findAll() {
        log.info("获取用户信息");
        return Result.success("获取用户信息成功",sysUserMapper.findAll());
    }

    @Override
    public Result login(LoginVo loginVo) {

        log.info("1.开始登录");
        UserDetails userDetails = userDetailsService.loadUserByUsername(loginVo.getUsername());

        log.info("判断账户密码");
        if (null == userDetails || !passwordEncoder.matches(loginVo.getPassword(),userDetails.getPassword())) {
            return Result.fail("账号或密码错误");
        }
        if (!userDetails.isEnabled()) {
            return Result.fail("该账号已禁用,请联系管理员");
        }
        log.info("登录成功，载security中存入登录信息");
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities() );
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        log.info("根据登录信息获取token");

        //借助jwt来生成token
        String token = tokenUtils.generateToken(userDetails);

        HashMap<String, String> map = new HashMap<>(2);
        map.put("tokenHead", tokenHead);
        map.put("token", token);

        return Result.success("登录成功!", map);
    }

    @Override
    public SysUser findByUsername(String username) {
        return sysUserMapper.findByUsername(username);
    }
}
