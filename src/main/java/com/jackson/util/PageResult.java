package com.jackson.util;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Data
public class PageResult<T> extends Result implements Serializable {
    private long total;

    private List<T> rows;

    public PageResult(long total, List<T> rows) {
        this.setFlag(true);
        this.setMessage("分页查询成功");
        this.total = total;
        this.rows = rows;
    }
}
