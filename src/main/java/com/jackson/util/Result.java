package com.jackson.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel(value = "响应参数")
@Data
public class Result implements Serializable {

    @ApiModelProperty(value = "是否响应成功", dataType = "boolean")
    private boolean flag;

    @ApiModelProperty(value = "响应信息", dataType = "响应数据")
    private String message;

    @ApiModelProperty(value = "响应数据", dataType = "Object")
    private Object data;

    public Result(boolean flag, String message, Object data) {
        this.flag = flag;
        this.message = message;
        this.data = data;
    }

    public Result() {
    }

    public Result(boolean flag, String message) {
        this.flag = flag;
        this.message = message;
    }

    /**
     *
     * @param message
     * @param data
     * @return
     */
    public static Result success(String message, Object data) {
        return new Result(true,message,data);
    }


    /**
     *
     * @param message
     * @return
     */
    public static Result fail(String message) {
        return new Result(false,message);
    }



    public static Result success(String message) {
        return new Result(true, message);
    }
}
