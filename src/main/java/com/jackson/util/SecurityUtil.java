package com.jackson.util;

import com.jackson.entity.SysUser;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @Description:获取当前登录用户的基本信息
 * @Author: Jackson
 * @Date: 2021/8/30 15:36
 */
public class SecurityUtil {


    /**
     * 从Security主题信息中获取用户信息
     * @return
     */
    public static SysUser getUser() {
        SysUser user = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        user.setPassword(null);
        return user;

    }


    /**
     * 获取用户名
     * @return
     */
    public static String getUsername() {
        return getUser().getUsername();
    }

    /**
     * 获取用户 id
     * @return
     */
    public static Long getUserId() {
        return getUser().getId();
    }
}
