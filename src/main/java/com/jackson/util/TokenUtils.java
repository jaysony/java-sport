package com.jackson.util;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;


@Component
public class TokenUtils {

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private long expiration;

    /**
     * 生成token
     * @param userDetails
     * @return
     */
    public String generateToken(UserDetails userDetails) {
        HashMap<String, Object> map = new HashMap<>(2);
        map.put("username", userDetails.getUsername());
        map.put("created", new Date());
        return this.generateJwt(map);

    }

    /**
     * 根据载荷信息生成token
     * @param map
     * @return
     */
    private String generateJwt(HashMap<String, Object> map) {
        return Jwts.builder()
                .setClaims(map)
                .signWith(SignatureAlgorithm.HS512,secret)
                .setExpiration(new Date(System.currentTimeMillis() + expiration *1000))
                .compact();
    }


    /**
     * 根据token获取载荷信息
     * @param token
     * @return
     */
    public Claims getTokenBody(String token) {
        try {
            return Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 根据token得到用户名
     * @param token
     * @return
     */
    public String getUsernameByToken(String token) {
        return  (String ) this.getTokenBody(token).get("username");
    }

    /**
     * 根据token判断是否过期
     * @param token
     * @return true说明过期
     */
    public Boolean isExpiration(String token) {
        return this.getTokenBody(token).getExpiration().before(new Date());

    }


    public String refreshToken (String token) {
        Claims claims = this.getTokenBody(token);
        claims.setExpiration(new Date());
        return this.generateJwt((HashMap<String, Object>) claims);

    }
}
