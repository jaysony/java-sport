package com.jackson.config;

import org.apache.catalina.filters.CorsFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        //允许访问的路径
        registry
                .addMapping("/**")
                //请求来源
                .allowedOrigins("http://localhost:8888","null")
                //允许访问的方法
                .allowedMethods("GET","POST","DELETE","PUT")
                //允许携带参数
                .allowCredentials(true)
                .allowedHeaders("*")
                //最大响应时间
                .maxAge(3600);
    }
}
