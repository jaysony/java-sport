package com.jackson.config.security.contents;

/**
 * @Description:
 * @Author: Jackson
 * @Date: 2021/8/29 17:03
 */
public class SecurityContents {
    public static final String[] WHITE_LIST = {
            "/user/login",
            "/test",
            //swagger相关
            "/swagger-ui.html",
            "/doc.html",
            "/webjars/**",
            "/swagger-resources/**",
            "/v2/**",
            "/configuration/ui",
            "/configuration/security",
    };
}
