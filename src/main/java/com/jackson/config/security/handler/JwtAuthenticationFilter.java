package com.jackson.config.security.handler;

import com.jackson.config.security.service.UserDetailServiceImpl;
import com.jackson.util.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * token认证
 * 在接口访问前进行校验
 */
@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private UserDetailServiceImpl userDetailsService;

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;

    @Value("${jwt.tokenHead}")
    private String tokenHead;


    /**
     * 请求前获取请求头信息
     * @param httpServletRequest
     * @param httpServletResponse
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        if (httpServletRequest.getHeader("Authorization") == null) {
            System.out.println("没有token");
        }

        //1.获取token
        String header = httpServletRequest.getHeader(tokenHeader);
        //2.判断token是否存在
        if (null != header && header.startsWith(tokenHead)) {
            //拿到token主体
            String token  = header.substring(tokenHead.length());
            //根据token获取用户名
            String username = tokenUtils.getUsernameByToken(token);
            //进行判断

            //token存在但没有登录信息
            if (null != username && null == SecurityContextHolder.getContext().getAuthentication()) {
                System.out.println("能获取token，但是没有登录信息");
                //没有登录信息 直接登录
                UserDetails userDetails = userDetailsService.loadUserByUsername(username);

                /*
                  判断token是否有效  感觉username.equals(userDetails.getUsername())这一步有点多余
                  TODO:你写的代码没有！导致这段代码没有执行，并且username.equals这里你也写错了，写成了userDetails
                 */
                if ( !tokenUtils.isExpiration(token) && username.equals(userDetails.getUsername())) {
                    System.out.println("能获取token，且未过期");
                    // ★刷新security中的用户信息 这三行代码很重要
                    // 1. 这里其实已经认定你登录上了，但是如何让系统验证通过就得通过 authenticationToken ，三个参数分别是 用户信息，凭证，权限
                    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails,null, userDetails.getAuthorities());
                    // 2. 这里把请求request放到认证token上了，不明觉厉，这个也不知道是什么SecurityContextHolder（估计是安全的全局配置啥的，存放登录信息，这样配置之后就不会去401）
                    authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                    SecurityContextHolder.getContext().setAuthentication(authenticationToken); // TODO：猜测：如果没有执行这几行就会抛出 AuthenticationException ，进而执行 401那块代码
                }
            }
        }
        //过滤器放行
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
