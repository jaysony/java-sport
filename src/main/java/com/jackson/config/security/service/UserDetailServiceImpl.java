package com.jackson.config.security.service;

import com.jackson.entity.SysMenu;
import com.jackson.entity.SysRole;
import com.jackson.entity.SysUser;
import com.jackson.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: UserDetailServiceImpl
 * @Description: 实现自定义登录逻辑
 * @Author: Jackson
 * @Date: 2021/8/29 14:37
 */
@Component
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private SysUserMapper userMapper;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser user = userMapper.findByUsername(username);
        if (null == user) {
            throw  new UsernameNotFoundException("用户名或密码错误");
        }
        if (user.isAdmin()) {
//            List<SysRole> list = new ArrayList<>();
//            SysRole role = new SysRole();
//            role.setCode("admin");
//            list.add(role);
//            user.setRoles(list);


            user.setRoles(userMapper.findRoles(null));
            user.setPermissions(userMapper.findPermissions(null));
            List<SysMenu> parentMenus = userMapper.findMenus(null);
            parentMenus.forEach( item -> {
                item.setChildren(userMapper.findChildrenMenus(item.getId(), null));

            });
            user.setMenus(parentMenus);

        } else {

            //非管理员需要查询角色信息
            user.setRoles(userMapper.findRoles(user.getId()));
            List<SysMenu> menus = userMapper.findMenus(null);
            menus.forEach( item -> {
                item.setChildren(userMapper.findChildrenMenus(item.getId(), user.getId()));
            });
            user.setMenus(menus);
            user.setPermissions(userMapper.findPermissions(user.getId()));
        }

        return user;
    }
}
