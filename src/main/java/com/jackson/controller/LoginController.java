package com.jackson.controller;

import com.jackson.service.SysUserService;
import com.jackson.util.Result;
import com.jackson.util.SecurityUtil;
import com.jackson.vo.LoginVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@Api(value = "用户使用接口")
@RestController
@RequestMapping("/user")
public class LoginController {

    @Autowired
    private SysUserService userService;


    @ApiOperation(value = "登录接口")
    @PostMapping("/login")
    public Result login(@RequestBody LoginVo loginVo) {
        return userService.login(loginVo);
    }

    @ApiOperation(value = "获取用户基本信息")
    @GetMapping("/getInfo")
    public Result getUserInfo() {
        System.out.println("这里是/user/getInfo，访问成功！");
        return Result.success("获取用户信息成功", SecurityUtil.getUser());
    }

    @ApiOperation(value = "退出登录")
    @GetMapping("/logout")
    public Result Logout() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return Result.success("退出成功");
    }
}
