package com.jackson.controller;

import com.jackson.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Api(value = "测试接口")
public class TestCtrl {


    @PreAuthorize("hasAuthority('USER_INSERT')")
    @ApiOperation(value = "测试test")
    //@PreAuthorize("hasAnyRole('admin')")
    @GetMapping("/test")
    public Result hello() {
        return Result.success("成功","你好");
    }



}
